module Control.Distributed.Process.Dict 
  ( module Control.Distributed.Process.Dict.Dict
  , module Control.Distributed.Process.Dict.HasDict
  , remoteTable
  
  ) where
  
import qualified Control.Distributed.Process.Dict.Dict as D
import qualified Control.Distributed.Process.Dict.StorableDict as SD
import qualified Control.Distributed.Process.Dict.HasDict as H 

import Control.Distributed.Process.Dict.Dict hiding (remoteTable)
import Control.Distributed.Process.Dict.StorableDict hiding (remoteTable)
import Control.Distributed.Process.Dict.HasDict hiding (__remoteTable)

import Control.Distributed.Process 
 
remoteTable :: RemoteTable -> RemoteTable
remoteTable = D.remoteTable . 
              SD.remoteTable .
              H.__remoteTable 
              
