module  Control.Distributed.Process.Dict.HasDict 
  ( HasDict(..)
  , HasStorableDict(..)
  , StorableDict (..)
  , deriveDicts 
  , deriveStorableDicts
  
  , staticApplyEnv
  , __remoteTable
  
  
   
  ) where
 

import Control.Distributed.Process.Serializable (Serializable)
import Control.Distributed.Process 
import Control.Distributed.Process.Closure
import Control.Distributed.Static 

import Data.Typeable
import Data.Binary

import qualified Data.Vector.Storable as S
import qualified Data.Vector as V

import Data.Vector.Binary ()

import Language.Haskell.TH 



staticApplyEnv  :: (HasDict a, Typeable b) => Static (a -> b) -> a -> Closure b
staticApplyEnv static env = closure decoder (encode env) where
    decoder = static `staticCompose` staticDecode staticDict


data StorableDict a where
    StorableDict :: (S.Storable a, Serializable a) => StorableDict a
  deriving Typeable

class Serializable a => HasDict a where  
  staticDict :: Static (SerializableDict a)
  
class (S.Storable a, Serializable a) => HasStorableDict a where  
  storableDict :: Static (StorableDict a)  
  
  
makeSVector ::  StorableDict a ->  SerializableDict (S.Vector a) 
makeSVector StorableDict = SerializableDict



makeVector ::  SerializableDict a -> SerializableDict (V.Vector a) 
makeVector  SerializableDict = SerializableDict  


makeMaybe ::  SerializableDict a -> SerializableDict (Maybe a) 
makeMaybe  SerializableDict = SerializableDict  


makePair :: SerializableDict a -> SerializableDict b -> SerializableDict (a, b)
makePair SerializableDict SerializableDict = SerializableDict

makeEither :: SerializableDict a -> SerializableDict b -> SerializableDict (Either a b)
makeEither SerializableDict SerializableDict = SerializableDict

makeTrip :: SerializableDict a -> SerializableDict b -> SerializableDict c -> SerializableDict (a, b, c)
makeTrip SerializableDict SerializableDict SerializableDict = SerializableDict

makeList :: SerializableDict a -> SerializableDict [a]
makeList SerializableDict = SerializableDict

makeSendPort :: SerializableDict a -> SerializableDict (SendPort a)
makeSendPort SerializableDict = SerializableDict

remotable ['makePair, 'makeTrip, 'makeList, 'makeEither, 'makeVector, 'makeMaybe, 'makeSVector, 'makeSendPort] 

 
instance (HasDict a, HasDict b) => HasDict (a, b) where
  staticDict = $(mkStatic 'makePair) `staticApply` staticDict `staticApply` staticDict
  
instance (HasDict a, HasDict b) => HasDict (Either a b) where
  staticDict = $(mkStatic 'makeEither) `staticApply` staticDict `staticApply` staticDict  

instance (HasDict a) => HasDict [a] where
  staticDict = $(mkStatic 'makeList) `staticApply` staticDict  

instance (HasStorableDict a, HasDict a) => HasDict (S.Vector a) where
  staticDict = $(mkStatic 'makeSVector) `staticApply` storableDict  
  
instance (HasDict a) => HasDict (V.Vector a) where
  staticDict = $(mkStatic 'makeVector) `staticApply` staticDict    

instance (HasDict a) => HasDict (Maybe a) where
  staticDict = $(mkStatic 'makeMaybe) `staticApply` staticDict     
  
instance (HasDict a) => HasDict (SendPort a) where
  staticDict = $(mkStatic 'makeSendPort) `staticApply` staticDict    

instance (HasDict a, HasDict b, HasDict c) => HasDict (a, b, c) where
  staticDict = $(mkStatic 'makeTrip) `staticApply` staticDict `staticApply` staticDict `staticApply` staticDict 


deriveDict :: Name -> Q [Dec]
deriveDict name = do
  
  let dictName = mkName ("dict_" ++ nameBase name)
  
  dict <- sequence [sigD dictName [t| SerializableDict $(conT name)|],
            valD (varP dictName) (normalB [e| SerializableDict |]) [] ] 
            
  inst <- [d| instance HasDict $(conT name) where 
                staticDict = $(mkStatic dictName) |]
  return (dict ++ inst) 
  
deriveStorableDict :: Name -> Q [Dec]
deriveStorableDict name = do
  
  let dictName = mkName ("dict_" ++ nameBase name)
  
  dict <- sequence [sigD dictName [t| StorableDict $(conT name)|],
            valD (varP dictName) (normalB [e| StorableDict |]) [] ] 
            
  inst <- [d| instance HasStorableDict $(conT name) where 
                storableDict = $(mkStatic dictName) |]
  return (dict ++ inst)   
        

deriveDicts :: [Name] -> Q [Dec]
deriveDicts names = remotableDecl (map deriveDict names) 
   
deriveStorableDicts :: [Name] -> Q [Dec]
deriveStorableDicts names = remotableDecl (map deriveStorableDict names) 




    