module  Control.Distributed.Process.Dict.Dict 
  ( staticDict
  , remoteTable
  , Serializable 
  , module Control.Distributed.Process.Dict.HasDict
   
  ) where
 

import Control.Distributed.Process.Serializable 
import Control.Distributed.Process 
 
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString as B
 

 
import Control.Distributed.Process.Dict.HasDict hiding (__remoteTable)

type LazyByteString = L.ByteString

deriveDicts [''Float, ''Double, ''Integer, ''Int, ''ProcessId, ''NodeId, ''Char, ''(), ''LazyByteString, ''B.ByteString]


remoteTable :: RemoteTable -> RemoteTable
remoteTable = __remoteTableDecl   