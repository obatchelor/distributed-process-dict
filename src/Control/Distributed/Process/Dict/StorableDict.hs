module Control.Distributed.Process.Dict.StorableDict 
  ( storableDict
  , remoteTable
  , Storable 
  , HasStorableDict
   
  ) where
 

import Control.Distributed.Process.Serializable 
import Control.Distributed.Process 
 
import Data.Vector.Storable (Storable)
 
import Control.Distributed.Process.Dict.HasDict


deriveStorableDicts [''Float, ''Double, ''Int, ''Char]


remoteTable :: RemoteTable -> RemoteTable
remoteTable = __remoteTableDecl   